package itacademy.com.show018;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitService {

    @GET("messages1.json")
    Call<List<MessageModel>> getMessageList();

}
