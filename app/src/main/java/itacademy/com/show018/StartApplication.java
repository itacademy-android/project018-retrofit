package itacademy.com.show018;

import android.app.Application;
import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StartApplication extends Application {
    private static final String BASE_URL = "https://rawgit.com/startandroid/data/master/messages/";
    private RetrofitService service;

    @Override
    public void onCreate() {
        super.onCreate();
        service = initRetrofit();
    }

    public static StartApplication get(Context context) {
        return (StartApplication) context.getApplicationContext();
    }

    public RetrofitService getRetrofitClient() {
        return service;
    }

    private RetrofitService initRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RetrofitService.class);
    }
}
