package itacademy.com.show018;

import com.google.gson.annotations.SerializedName;

public class MessageModel {
    private int id;
    private long time;

    @SerializedName("text")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /*public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }*/
}
