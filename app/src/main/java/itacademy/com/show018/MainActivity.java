package itacademy.com.show018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        Button btnFetch = findViewById(R.id.btnFetch);
        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest();
            }
        });
    }

    private void sendRequest() {
        RetrofitService service = StartApplication.get(this).getRetrofitClient();
        Call<List<MessageModel>> call = service.getMessageList();
        call.enqueue(new Callback<List<MessageModel>>() {
            @Override
            public void onResponse(Call<List<MessageModel>> call, Response<List<MessageModel>> response) {

                List<MessageModel> list = response.body();

                ArrayList<String> stringList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    stringList.add(list.get(i).getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                        android.R.layout.simple_list_item_1, stringList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<MessageModel>> call, Throwable t) {
                Log.d("ERROR", t.getMessage());
            }
        });
    }
}
